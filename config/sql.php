<?php

require __DIR__ . '/../models/users.php';
require __DIR__ . '/../models/products.php';
require __DIR__ . '/../models/shop.php';

// =============================================================================
// = Users
// =============================================================================

/*獲取使用者所有欄位名稱*/

function fetchAllUsersField($conn)
{
    $stmt = $conn->prepare('SHOW COLUMNS FROM `users`');
    $stmt->execute();

    $columns = $stmt->fetchAll(PDO::FETCH_ASSOC);

    function fieldName($column)
    {
        return $column['Field'];
    }

    return array_map('fieldName', $columns);
}

/*取得所有使用者*/

function fetchAllUser($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `users`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Users');
}

/*依照給予的帳號，取得使用者*/

function findUserByAccount($conn, $account)
{
    $stmt = $conn->prepare("SELECT * FROM `users` WHERE account = :account");
    $stmt->execute(["account" => $account]);

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

/*依照給予的欄位與關鍵字，取得符合的使用者*/

function findUserLikeSearch($conn, $search, $field, $sort)
{
    $sql = "SELECT * FROM `users` WHERE `{$field}` like :search AND `deleted_at` IS NULL ORDER BY `{$sort}` ASC";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["search" => "%{$search}%"]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Users');
}

/*用ID取得使用找資料*/

function findUserById($conn, $id)
{
    $sql = "SELECT * FROM `users` WHERE `id`=:id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["id" => $id ]);

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

/*新增使用者*/

function createUser($conn, $data = [])
{

    $sql = "INSERT INTO users (role, account, password, name, created_at, updated_at) VALUES (:role, :account, :password, :name, :created_at, :updated_at) ";
    $stmt = $conn->prepare($sql);
    $addUserData = [
        'role'       => $data['role'] ?? 'C',
        'account'    => $data['account'],
        'password'   => $data['password'],
        'name'       => $data['name'],
        'created_at' => $data['created_at'] ?? date('Y-m-d'),
        'updated_at' => $data['updated_at'] ?? null
    ];

    return $stmt->execute($addUserData);
}

/*修改使用者資料*/

function updateUser($conn, $id, $data = [])
{

    $sql = "update users set account=:account, password=:password, name=:name, updated_at=:updated_at where id={$id}";
    $stmt = $conn->prepare($sql);
    $updateUserData = [
        'account'    => $data['account'],
        'password'   => $data['password'],
        'name'       => $data['name'],
        'updated_at' => $data['updated_at'] ?? date('Y-m-d H:i:s'),
    ];
    $stmt->execute($updateUserData);

    return $stmt->execute($updateUserData);
}

/*軟刪除使用者*/

function deleteusers($conn, $id)
{
    $stmt = $conn->prepare(
        "UPDATE `users` SET `deleted_at`= CURRENT_TIME() WHERE `id`={$id}"
    );

    return $stmt->execute();
}

// =============================================================================
// = Products & Suppliers
// =============================================================================

/*產品搜尋*/

function findProductLikeSearch($conn, $search, $field, $sort)
{
    $sql = "SELECT * FROM `product` WHERE `{$field}` like :search AND `deleted_at` IS NULL ORDER BY `{$sort}` ASC";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["search" => "%{$search}%"]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*產品搜尋*/

function findProductLikeSearches($conn, $search)
{
    $sql = "SELECT * FROM `product` WHERE `product_name` like :search AND `deleted_at` IS NULL";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["search" => "%{$search}%"]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*取得所有產品*/

function fetchAllProducts($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `product`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*取得所有供應商*/

function fetchAllSuppliers($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `supply`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Suppliers');
}

/*取得供應商="S001"的產品*/

function fetchLikeProducts1($conn)
{
    $stmt = $conn->prepare("SELECT * FROM `product` WHERE `sup_id`='S001'");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*取得供應商="S002"的產品*/

function fetchLikeProducts2($conn)
{
    $stmt = $conn->prepare("SELECT * FROM `product` WHERE `sup_id`='S002'");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*取得供應商="S003"的產品*/

function fetchLikeProducts3($conn)
{
    $stmt = $conn->prepare("SELECT * FROM `product` WHERE `sup_id`='S003'");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*取得供應商="S004"的產品*/

function fetchLikeProducts4($conn)
{
    $stmt = $conn->prepare("SELECT * FROM `product` WHERE `sup_id`='S004'");
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*用Id取得產品資料*/

function findProductById($conn, $id)
{
    $sql = "SELECT * FROM `product` WHERE `product_id`=:id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["id" => $id ]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Products');
}

/*用Id取得產品資料*/

function findProductByIds($conn, $id)
{
    $sql = "SELECT * FROM `product` WHERE `id`=:id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["id" => $id ]);

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

/*新增產品*/

function createProduct($conn, $data = [])
{

    $sql = "INSERT INTO product (product_id, product_name, product_size, product_amount, price, updated_at, deleted_at) VALUES (:product_id, :product_name, :product_size, :product_amount, :price, :updated_at, :deleted_at) ";
    $stmt = $conn->prepare($sql);
    $addproductData = [
        'product_id'     => $data['product_id'],
        'product_name'   => $data['product_name'],
        'product_size'   => $data['product_size'],
        'product_amount' => $data['product_amount'],
        'price'          => $data['price'],
        'updated_at'     => $data['updated_at'] ?? null,
        'deleted_at'     => $data['deleted_at'] ?? null
    ];

    return $stmt->execute($addproductData);
}

/*修改產品資料*/

function updateProduct($conn, $id, $data = [])
{

    $sql = "update product set product_id=:product_id, product_name=:product_name, product_size=:product_size, product_amount=:product_amount, price=:price, updated_at=:updated_at where id={$id}";
    $stmt = $conn->prepare($sql);
    $updateproductData = [
        'product_id'     => $data['product_id'],
        'product_name'   => $data['product_name'],
        'product_size'   => $data['product_size'],
        'product_amount' => $data['product_amount'],
        'price'          => $data['price'],
        'updated_at'     => $data['updated_at'] ?? date('Y-m-d H:i:s'),
    ];
    $stmt->execute($updateproductData);

    return $stmt->execute($updateproductData);
}

/*軟刪除產品*/

function deleteProduct($conn, $id)
{
    $stmt = $conn->prepare(
        "UPDATE `product` SET `deleted_at`= CURRENT_TIME() WHERE `id`={$id}"
    );

    return $stmt->execute();
}


// =============================================================================
// = Shops
// =============================================================================

/*獲取商店所有欄位名稱*/

function fetchAllShopsField($conn)
{
    $stmt = $conn->prepare('SHOW COLUMNS FROM `shop`');
    $stmt->execute();

    $columns = $stmt->fetchAll(PDO::FETCH_ASSOC);

    function fieldName1($column)
    {
        return $column['Field'];
    }

    return array_map('fieldName', $columns);
}

/*取得所有商店資料*/

function fetchAllShops($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `shop`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Shops');
}

/*用Id取得商店資料*/

function findShopById($conn, $id)
{
    $sql = "SELECT * FROM `shop` WHERE `id`=:id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["id" => $id ]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Shops');
}

/*用Id取得商店資料*/

function findShopByIds($conn, $id)
{
    $sql = "SELECT * FROM `shop` WHERE `id`=:id ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["id" => $id ]);

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function findShopByName($conn, $name)
{
    $sql = "SELECT * FROM `shop` WHERE `shop_name`=:name ";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["name" => $name ]);

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/*搜尋商店*/

function findShopLikeSearch($conn, $search)
{
    $sql = "SELECT * FROM `shop` WHERE `shop_name` like :search";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["search" => "%{$search}%"]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Shops');
}

/*搜尋商店&排序*/

function findShopLikeSearches($conn, $search, $field, $sort)
{
    $sql = "SELECT * FROM `shop` WHERE `{$field}` like :search AND `deleted_at` IS NULL ORDER BY `{$sort}` ASC";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["search" => "%{$search}%"]);

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Shops');
}

/*新增商店*/

function createShop($conn, $data = [])
{

    $sql = "INSERT INTO shop (shop_id, shop_name, shop_address, shop_phone, updated_at, deleted_at) VALUES (:shop_id, :shop_name, :shop_address, :shop_phone, :updated_at, :deleted_at) ";
    $stmt = $conn->prepare($sql);
    $addShopData = [
        'shop_id'       => $data['shop_id'],
        'shop_name'     => $data['shop_name'],
        'shop_address'  => $data['shop_address'],
        'shop_phone'    => $data['shop_phone'],
        'updated_at'    => $data['updated_at'] ?? null,
        'deleted_at'    => $data['deleted_at'] ?? null
    ];

    return $stmt->execute($addShopData);
}

/*修改商店資料*/

function updateShop($conn, $id, $data = [])
{

    $sql = "update shop set shop_id=:shop_id, shop_name=:shop_name, shop_address=:shop_address, shop_phone=:shop_phone, updated_at=:updated_at where id={$id}";
    $stmt = $conn->prepare($sql);
    $updateShopData = [
        'shop_id'      => $data['shop_id'],
        'shop_name'    => $data['shop_name'],
        'shop_address' => $data['shop_address'],
        'shop_phone'   => $data['shop_phone'],
        'updated_at'   => $data['updated_at'] ?? date('Y-m-d H:i:s'),
    ];
    $stmt->execute($updateShopData);

    return $stmt->execute($updateShopData);
}

/*軟刪除商店*/

function deleteShop($conn, $id)
{
    $stmt = $conn->prepare(
        "UPDATE `shop` SET `deleted_at`= CURRENT_TIME() WHERE `id`={$id}"
    );

    return $stmt->execute();
}


// =============================================================================
// 跨資料表讀取
// =============================================================================

/*跨資料表查詢-商店_商品*/

function findProSupLikeSearch($conn)
{
    $sql = "SELECT supply.id,supply.sup_id,sup_name,COUNT(product.product_id)\n"

    . "FROM product, supply\n"

    . "WHERE product.sup_id=supply.sup_id\n"

    . "GROUP BY supply.sup_id";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/*跨資料表查詢-商店_員工*/

function findShopEmpLikeSearch($conn)
{
    $sql = "SELECT shop.id,shop.shop_id,shop.shop_name,COUNT(employee.emp_id)\n"

    . "FROM shop,employee\n"

    . "WHERE shop.shop_id=employee.shop_id\n"

    . "GROUP BY shop.id,shop.shop_id,shop.shop_name";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}