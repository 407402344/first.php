<?php

require __DIR__ . '/etc/bootstrap.php';

//非管理者則跳回貼圖商店
if ($_SESSION["authority"] != "M") {
    header("Location:index-sup.php");
    die;
}

//獲取管理者名稱
$name = $_SESSION["name"] ?? "";

//供應商供應商品數
$Pro_SupTitle = [
    "id"              => "編號",
    "sup_id"          => "供應商編號",
    "sup_name"        => "供應商名稱",
    "product_count"   => "供應產品數",
];

$pro_supList = findProSupLikeSearch($conn);

//分店員工數
$Shop_EmpTitle = [
    "id"          => "編號",
    "shop_id"     => "分店編號",
    "shop_name"   => "分店名稱",
    "emp_count"   => "員工人數",
];

$Shop_EmpList = findShopEmpLikeSearch($conn);
?>

<html>

<?php include("header.php") ?>

<body class="page__userTable">
    <div class="wrapper">

        <header class="header">
            <h1 class="header__logo">
                <a href="index-sup.php"><span>WEB</span>STORE</a>
            </h1>
            
            <ul class="header__util">
                <li class="header__util_item wish-box">
                    <a><span>你好，<?= $name ?></span></a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="index-sup.php">
                        <span>返回商店</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="managepage.php">
                        <span>註冊清單管理</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="shopmanage.php">
                        <span>分店管理</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="productmanage.php">
                        <span>產品管理</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item login-button">
                    <a href="process/logout_process.php">登出</a>
                </li>
            </ul>
        </header>

        <div class="content">

        <!-- 分店員工數 -->
        <div class="class__paper">
                <h2>各分店數一覽</h2>
                <table class="class__table">
                    <thead class="class__table_head">
                        <tr class="class__table_row">
                            <?php foreach ($Shop_EmpTitle as $key => $title) { ?>
                                <th class="class__table_cell class__table_cell--head">
                                    <a class="icon table__cell_button">
                                        <?= $title ?>
                                    </a>
                                </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody class="class__table_content">
                        <?php foreach ($Shop_EmpList as $shop_emp) { ?>
                            <tr class="class__table_row class__table_row--body">
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $shop_emp["id"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $shop_emp["shop_id"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $shop_emp["shop_name"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $shop_emp["COUNT(employee.emp_id)"] ?>
                                    </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- 供應商供應商品數 -->
            <div class="class__paper">
                <h2>供應商供應商品數一覽</h2>
                <table class="class__table">
                    <thead class="class__table_head">
                        <tr class="class__table_row">
                            <?php foreach ($Pro_SupTitle as $key => $title) { ?>
                                <th class="class__table_cell class__table_cell--head">
                                    <a class="icon table__cell_button">
                                        <?= $title ?>
                                    </a>
                                </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody class="class__table_content">
                        <?php foreach ($pro_supList as $pro_sup) { ?>
                            <tr class="class__table_row class__table_row--body">
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $pro_sup["id"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $pro_sup["sup_id"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $pro_sup["sup_name"] ?>
                                    </td>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $pro_sup["COUNT(product.product_id)"] ?>
                                    </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
</body>

</html>