<?php

require __DIR__ . '/../etc/bootstrap.php';

//register new User 
if (!empty($_POST)) {

    // =============================================================================
    // = 處理送來的表單資料
    // =============================================================================

    $uId = $_POST["id"] ?? "";
    $ushopid = $_POST["shop_id"] ?? "";
    $uName = $_POST["shop_name"] ?? "";
    $uAddress = $_POST["shop_address"] ?? "";
    $uPhone = $_POST["shop_phone"] ?? "";

    /* =============================================================================
     * = 確認店家是否存在
     * =============================================================================
    **/

    $shop = findShopById($conn, $ushopid);

    if ($shop) {
        header("Location:../shopmanage.php?msg=店家已存在");
        die;
    }

    /* =============================================================================
     * = 新增使用者
     * =============================================================================
    **/

    $addResult = createShop($conn, [
        'shop_id' => $ushopid,
        'shop_name' => $uName,
        'shop_address' => $uAddress,
        'shop_phone' => $uPhone,
    ]);

    if ($addResult=="1") {


        
        if ($_FILES['fileToUpload']['tmp_name'] != "") {
            $type = $_FILES['fileToUpload']['type'];
            $size = $_FILES['fileToUpload']['size'];
            $name = $_FILES['fileToUpload']['name'];
            $tmp_name = $_FILES['fileToUpload']['tmp_name'];
            $newshop = findShopByName($conn, $uName);
            $sizemb = round($size / 1024000, 2);
            if ($type == "image/jpeg") {
                if ($sizemb < 3) {
                    $file = explode(".", $name);
                    foreach ($newshop as $newshop){
                    $new_name = $newshop["id"];
                    }
                    move_uploaded_file($tmp_name, "../img/shop/" . $new_name . "." . $file[1]);
                    $photo = "圖片上傳成功";
                } else {
                    $photo = "圖片太大，上傳失敗";
                }
            } else {
                $photo = "檔案格式錯誤，上傳失敗";
            }
        } else {
            $photo = "";
        }
    }

    // 跳轉並將結果帶回註冊頁面。
    header("Location:../shopmanage.php?result={$addResult}&photo={$photo}");
    die();
}

header("Location:../shopmanage.php");
