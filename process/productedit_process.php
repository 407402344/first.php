<?php

require __DIR__ . '/../etc/bootstrap.php';

//確認是否有修改表單資料
if (!empty($_POST)) {

    // =============================================================================
    // = 處理送來的表單資料
    // =============================================================================

    $uId = $_POST["id"] ?? "";
    $uproductid = $_POST["product_id"] ?? "";
    $uName = $_POST["product_name"] ?? "";
    $usize = $_POST["product_size"] ?? "";
    $uamount = $_POST["product_amount"] ?? "";
    $uprice = $_POST["price"] ?? "";

    /* =============================================================================
     * = 修改使用者資料
     * =============================================================================
    **/

    $updateResult = updateproduct($conn, $uId, [
        'product_id'     => $uproductid,
        'product_name'   => $uName,
        'product_size'   => $usize,
        'product_amount' => $uamount,
        'price'          => $uprice,
    ]);

    $key = $uId -1;
    // 跳轉並將結果帶回修改頁面。
    header("Location:../productmanage.php?edit={$uId}&result={$updateResult}");
    die();
}

header("Location:productmanage.php");
