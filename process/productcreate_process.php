<?php

require __DIR__ . '/../etc/bootstrap.php';

//register new User 
if (!empty($_POST)) {

    // =============================================================================
    // = 處理送來的表單資料
    // =============================================================================

    $uproductid = $_POST["product_id"] ?? "";
    $uName = $_POST["product_name"] ?? "";
    $uSize = $_POST["product_size"] ?? "";
    $uAmount = $_POST["product_amount"] ?? "";
    $uPrice = $_POST["price"] ?? "";

    /* =============================================================================
     * = 確認店家是否存在
     * =============================================================================
    **/

    $product = findproductById($conn, $uproductid);

    if ($product) {
        header("Location:../productmanage.php?msg=店家已存在");
        die;
    }

    /* =============================================================================
     * = 新增使用者
     * =============================================================================
    **/

    $addResult = createproduct($conn, [
        'product_id'     => $uproductid,
        'product_name'   => $uName,
        'product_size'   => $uSize,
        'product_amount' => $uAmount,
        'price'          => $uPrice,
    ]);

    // 跳轉並將結果帶回註冊頁面。
    header("Location:../productmanage.php?result={$addResult}");
    die();
}

header("Location:../productmanage.php");
