<?php

session_start();

//接收註冊狀態訊息
$msg = $_GET["msg"] ?? "";
if(isset($_GET["result"])){ 
  $msg = $_GET["result"] ? "帳號註冊成功" : "帳號註冊失敗";
};

?>

<!DOCTYPE html>

<html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Register</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>


<section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

        <div class="section-header">
          <h3>WEB Register </h3>
          
        </div>

              <center> 
                  <p><?= $msg ?></p>
              </center> 
          
                <form name="loginForm" action="process/register_process.php" method="post">
                    <div class="form">
                    <div class="form-row">

              <center>
                  <div class="form-group col-md-6">
                    <input type="text"  class="form-control" name="account" placeholder="請輸入註冊帳號" required autocapitalize="off" autocorrect="off" spellcheck="false" />
                    <div class="validation"></div>
                  </div>
                  
                  <div class="form-group col-md-6">
                    <input type="text" class="form-control" name="password"  placeholder="請輸入註冊密碼" required/>
                    <div class="validation"></div>
                  </div>
            
                  <div class="form-group">
                    <input type="text" class="form-control" name="name"  placeholder="請輸入使用者名稱" required/>
                    <div class="validation"></div>
                  </div>

                </center>
            
            </div>
            
            
                <center>

                  <br><div class="text-center"><button type="submit">註冊</button></div><br>

                  <div class="text-center"><a href="login.php" class="board__text_link">已有帳號？前往登入</a></div>
            
                  </center>

            
          </form>
        </div>

          </div>
          
    </section>


          
 

</body>

</html>