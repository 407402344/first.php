-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.8-MariaDB
-- PHP 版本： 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `web_final`
--

-- --------------------------------------------------------

--
-- 資料表結構 `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `emp_id` char(10) NOT NULL,
  `emp_name` varchar(45) NOT NULL,
  `emp_num` varchar(45) NOT NULL,
  `emp_phone` varchar(10) NOT NULL,
  `emp_gender` varchar(45) NOT NULL,
  `shop_id` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `employee`
--

INSERT INTO `employee` (`id`, `emp_id`, `emp_name`, `emp_num`, `emp_phone`, `emp_gender`, `shop_id`) VALUES
(1, 'E001', 'Amy', 'A123456789', '0919350940', 'F', 'SH001'),
(2, 'E002', 'Ben', 'A123456789', '0912345567', 'M', 'SH002'),
(3, 'E003', 'willy', 'A123456789', '0912345678', 'M', 'SH003'),
(4, 'E004', 'Emily', 'A123456789', '0912345678', 'F', 'SH004'),
(5, 'E005', 'Leo', 'D589403493', '0934561341', 'M', 'SH004'),
(6, 'E006', 'Momo', 'S120485734', '0962378453', 'F', 'SH005'),
(7, 'E007', 'Nick', 'C039485732', '0948324751', 'M', 'SH006'),
(8, 'E008', 'Oscar', 'S123456789', '0912345678', 'M', 'SH006'),
(9, 'E009', 'Peter', 'Q321567898', '0923456127', 'M', 'SH003'),
(10, 'E010', 'Queen', 'W123456789', '0912345678', 'F', 'SH001');

-- --------------------------------------------------------

--
-- 資料表結構 `equipment`
--

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL,
  `equip_id` char(10) NOT NULL,
  `equip_color` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `equipment`
--

INSERT INTO `equipment` (`id`, `equip_id`, `equip_color`) VALUES
(1, 'C001', 'black'),
(2, 'C002', 'blue'),
(3, 'C003', 'black'),
(4, 'C004', 'blue');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `mem_id` char(10) NOT NULL,
  `mem_name` varchar(45) NOT NULL,
  `mem_phone` char(10) NOT NULL,
  `mem_gender` varchar(45) NOT NULL,
  `mem_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `member`
--

INSERT INTO `member` (`id`, `mem_id`, `mem_name`, `mem_phone`, `mem_gender`, `mem_date`) VALUES
(1, 'M001', 'Alley', '0963428956', 'F', '2019-12-03'),
(2, 'M002', 'Ben', '0923465893', 'M', '2019-12-13'),
(3, 'M003', 'Clair', '0928375463', 'F', '2019-11-12'),
(4, 'M004', 'David', '0987235378', 'M', '2019-12-01'),
(5, 'M005', 'Eric', '0987325623', 'M', '2019-11-11'),
(6, 'M006', 'Frank', '0937856354', 'M', '2019-11-21'),
(7, 'M007', 'Grace', '0916472393', 'F', '2019-11-12'),
(8, 'M008', 'Hank', '0921568744', 'M', '2019-11-08'),
(9, 'M009', 'Iris', '0937286153', 'F', '2019-10-08'),
(10, 'M010', 'Jennie', '0916873234', 'F', '2019-10-29');

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `id` int(45) NOT NULL,
  `product_id` char(10) NOT NULL,
  `product_name` varchar(45) NOT NULL,
  `product_size` varchar(3) NOT NULL,
  `product_amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `sup_id` char(10) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `product`
--

INSERT INTO `product` (`id`, `product_id`, `product_name`, `product_size`, `product_amount`, `price`, `sup_id`, `updated_at`, `deleted_at`) VALUES
(1, 'P001', 'OLD SKOOL DX', '24', 5, 2180, 'S004', NULL, NULL),
(2, 'P001', 'OLD SKOOL DX', '25', 8, 2180, 'S004', '2019-12-30', NULL),
(3, 'P002', 'COURT ROYALE AC', '6.5', 3, 2000, 'S002', NULL, NULL),
(4, 'P002', 'COURT ROYALE AC', '7', 6, 2000, 'S002', NULL, NULL),
(5, 'P003', 'STAN SMITH', '5.5', 7, 1990, 'S001', NULL, NULL),
(6, 'P003', 'STAN SMITH', '6', 4, 1990, 'S001', NULL, NULL),
(7, 'P004', 'JP PRO OX', '8.5', 7, 2680, 'S003', NULL, NULL),
(8, 'P004', 'JP PRO OX', '9', 8, 2680, 'S003', NULL, NULL),
(9, 'P005', 'CHUCK TAYLOR ALL STAR', '11', 6, 800, 'S003', NULL, NULL),
(10, 'P005', 'CHUCK TAYLOR ALL STAR', '12', 8, 800, 'S003', NULL, NULL),
(11, 'P006', 'UA BESS NI', '6.5', 9, 1900, 'S004', NULL, NULL),
(12, 'P006', 'UA BESS NI', '7', 7, 1900, 'S004', NULL, NULL),
(13, 'P007', 'ADILETTE SHOWER', '8', 4, 890, 'S001', NULL, NULL),
(14, 'P007', 'ADILETTE SHOWER', '9', 7, 890, 'S001', NULL, NULL),
(15, 'P008', 'AIR ZOOM PEGASUS 36', '7', 5, 4000, 'S002', NULL, NULL),
(16, 'P008', 'AIR ZOOM PEGASUS 36', '7.5', 8, 4000, 'S002', NULL, '2019-12-30');

-- --------------------------------------------------------

--
-- 資料表結構 `shop`
--

CREATE TABLE `shop` (
  `id` int(45) NOT NULL,
  `shop_id` char(10) NOT NULL,
  `shop_name` varchar(45) NOT NULL,
  `shop_address` varchar(50) NOT NULL,
  `shop_phone` varchar(45) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `shop`
--

INSERT INTO `shop` (`id`, `shop_id`, `shop_name`, `shop_address`, `shop_phone`, `updated_at`, `deleted_at`) VALUES
(1, 'SH001', '信義ATT店', '台北市信義區松壽路12號3樓', '0277097395', NULL, NULL),
(2, 'SH002', '敦南店', '台北市大安區敦化南路一段187巷15號', '0277097615', '2019-12-29', NULL),
(3, 'SH003', '西門店', '台北市萬華區武昌街二段51號1樓', '0277097695', NULL, NULL),
(4, 'SH004', '信義微風店', '台北市信義區忠孝東路五段68號B2', '0277098755', NULL, NULL),
(5, 'SH005', '北車微風店', '台北市中正區北平西路3號B1', '0277300905', NULL, NULL),
(6, 'SH006', '內湖潤泰店', '台北市內湖區成功路四段188號1樓', '0277098075', '2019-12-30', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `supply`
--

CREATE TABLE `supply` (
  `id` int(11) NOT NULL,
  `sup_id` char(10) NOT NULL,
  `sup_name` varchar(45) NOT NULL,
  `sup_address` varchar(60) NOT NULL,
  `sup_phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `supply`
--

INSERT INTO `supply` (`id`, `sup_id`, `sup_name`, `sup_address`, `sup_phone`) VALUES
(1, 'S001', 'Adidas', 'taipei', '0912345678'),
(2, 'S002', 'Nike', 'taipei', '0912345678'),
(3, 'S003', 'Converse', 'taipei', '0912345678'),
(4, 'S004', 'Vans', 'taipei', '0912345678');

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` varchar(11) NOT NULL,
  `account` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `users`
--

INSERT INTO `users` (`id`, `role`, `account`, `password`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'M', '407402095', '095', '楊綵翔', '2019-12-19', NULL, NULL),
(2, 'M', '407402277', '277', '池怡蓁', '2019-12-19', NULL, NULL),
(3, 'M', '407402344', '344', '謝雨絜', '2019-12-19', NULL, NULL),
(4, 'M', '407402394', '394', '黃郁芳', '2019-12-19', NULL, NULL),
(5, 'C', 'tiny', 'tiny', 'tiny', '2019-12-20', '2019-12-28', NULL),
(6, 'C', 'cindy', 'cindy', 'cindy', '2019-12-20', NULL, NULL),
(7, 'C', 'tom', 'tom', 'tom', '2019-12-20', NULL, NULL),
(8, 'C', 'jojo', 'jojo', 'jojo', '2019-12-20', '2019-12-30', NULL),
(9, 'C', 'YY', 'YY', 'YY', '2019-12-30', NULL, NULL);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_id` (`shop_id`);

--
-- 資料表索引 `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sup_id` (`sup_id`);

--
-- 資料表索引 `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sup_id` (`sup_id`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `product`
--
ALTER TABLE `product`
  MODIFY `id` int(45) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(45) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `supply`
--
ALTER TABLE `supply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
