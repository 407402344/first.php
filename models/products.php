<?php
class Products {
   
  private $id;
  private $product_id;
  private $product_name;
  private $product_size; 
  private $product_amount;
  private $price;
  private $updated_at;
  private $deleted_at;


  function __set($variable, $value){}
    
  function __get($variable){  
    return $this->$variable;
  }

  /* constructor */
  function __construct(){

    $arguments = func_get_args();
    if (sizeof(func_get_args()) == 8){
      $this->id = $arguments["id"];
      $this->product_id = $arguments["product_id"];
      $this->product_name = $arguments["product_name"];
      $this->product_size = $arguments["product_size"];
      $this->product_amount = $arguments["product_amount"];
      $this->price = $arguments["price"];
      $this->updated_at = $arguments["updated_at"];
      $this->deleted_at = $arguments["deleted_at"];
    }
  }
  
}

class Suppliers {
   
  private $id;
  private $sup_id;
  private $sup_name;
  private $sup_address; 
  private $sup_phone;


  function __set($variable, $value){}
    
  function __get($variable){  
    return $this->$variable;
  }

  /* constructor */
  function __construct(){

    $arguments = func_get_args();
    if (sizeof(func_get_args()) == 5){
      $this->id = $arguments["id"];
      $this->sup_id = $arguments["sup_id"];
      $this->sup_name = $arguments["sup_name"];
      $this->sup_address = $arguments["sup_address"];
      $this->sup_phone = $arguments["sup_phone"];
    }
  }
  
}
?>
