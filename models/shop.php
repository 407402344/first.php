<?php
class Shops {
   
  private $id;
  private $shop_id;
  private $shop_name;
  private $shop_address;
  private $shop_phone;
  private $updated_at;
  private $deleted_at;
  

  function __set($variable, $value){}
  
  function __get($variable){  
    return $this->$variable;
  }

  /* constructor */

  function __construct(){

    $arguments = func_get_args();
    if (sizeof(func_get_args()) == 7){
        
      $this->id = $arguments["id"];
      $this->shop_id = $arguments["shop_id"];
      $this->shop_name = $arguments["shop_name"];
      $this->shop_address = $arguments["shop_address"];
      $this->shop_phone = $arguments["shop_phone"];
      $this->updated_at = $arguments["updated_at"];
      $this->deleted_at = $arguments["deleted_at"];
    }
  }

}

?>