<?php

require __DIR__ . '/etc/bootstrap.php';

//獲取所有供應商
$suppliers = fetchAllSuppliers($conn);

//搜尋產品及獲取當前選取的產品
$search = $_GET["search"] ?? "";
$field  = $_GET["supplier"] ?? "0";

$supplierId = $_GET["supplier"] ?? "0";
if ($supplierId == 1) {
    $products = fetchLikeProducts1($conn);
} elseif ($supplierId == 2) {
    $products = fetchLikeProducts2($conn);
} elseif ($supplierId == 3) {
    $products = fetchLikeProducts3($conn);
} elseif ($supplierId == 4) {
    $products = fetchLikeProducts4($conn);
} elseif ($supplierId == 0) {
    $products = findProductLikeSearches($conn, $search);
}

//獲取登入資訊 帳號/名稱/身份:預設'顧客'
$account = $_SESSION["account"] ?? "";
$name = $_SESSION["name"] ?? "";
$authority = $_SESSION["authority"] ?? "C";

?>

<html>

<?php include("header.php") ?>

<body class="page__sticker">
    <div class="wrapper">

        <header class="header">
            <h1 class="header__logo">
                <a href="index-sup.php"><span>WEB</span>STORE</a>
            </h1>
            <div class="header__search" data-widget="SearchBox">
                <form action="index-sup.php" method="GET">
                    <span class="header__search_block header__search_block--filter">
                        <i class="material-icons icon-filter">filter_list</i>
                        <select class="search__filter_select" name="supplier">
                            <option value="0">名稱</option>
                        </select>
                    </span>
                    <span class="header__search_block">
                        <input class="search__submit_input" type="submit" value="">
                        <i class="material-icons icon-search">search</i>
                        </input>
                    </span>
                    <input class="header__search_input" type="text" name="search" placeholder="搜尋產品" value="<?= $search ?>">
                </form>
            </div>
            <ul class="header__util">
                <!-- HW 驗證登入 並顯示帳號 -->
                <?php if (isset($_SESSION["name"])) { ?>
                    <li class="header__util_item wish-box">
                        <a><span>你好，<?= $name ?></span></a>
                        <span class="util__item_line">|</span>
                    </li>
                <?php } ?>
                <!-- 驗證登入 並顯示帳號 HW-->
                <!-- HW 驗證登入帳號是否為管理者 -->
                <?php if ($authority == "M") { ?>
                    <li class="header__util_item">
                        <a href="managepage.php">
                            <span>管理註冊清單</span>
                        </a>
                        <span class="util__item_line">|</span>
                    </li>
                    <!-- 驗證登入帳號是否為使用者 HW -->
                <?php } else { ?>
                    <li class="header__util_item wish-box">
                        <a href="#">
                            <span class="util__item_icon">
                                <i class="material-icons icon-wish">favorite_border</i>
                            </span>
                            <span>願望清單</span>
                        </a>
                        <span class="util__item_line">|</span>
                    </li>
                <?php } ?>
                <!-- HW 驗證是否登入 顯示不同按鈕 -->
                <?php if (isset($_SESSION["name"])) { ?>
                    <li class="header__util_item login-button">
                        <a href="process/logout_process.php">登出</a>
                    </li>
                <?php } else { ?>
                    <li class="header__util_item login-button">
                        <a href="login.php">登入</a>
                    </li>
                <?php } ?>
                <!-- 驗證是否登入 顯示不同按鈕 HW -->
            </ul>
        </header>

        <div class="content">
            <div class="sidebar">
                <nav class="sidebar__nav" role="navigation">
                    <h2 class="sidebar__title">商品一覽</h2>
                    <ul class="sidebar__list">
                        <li class="sidebar__list_item <?php if($supplierId==0){ echo "selected";}?>">
                            <a href="index-sup.php?supplier=0">
                                全部商品
                            </a>
                        </li>
                        <?php foreach ($suppliers as $suppliers) { ?>
                            <li class="sidebar__list_item <?php echo ($suppliers->id == $supplierId) ? "selected" : "" ?>">

                                <a href="index-sup.php?supplier=<?= $suppliers->id ?>">
                                    <?= $suppliers->sup_name ?>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="sidebar__list_item">
                            <a href="index-of-shop.php">
                                商店資訊
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="main">
                <section style="width: 100%" class="main__section">

                    <div class="endTop__block_head">
                        <!-- STICKER TITLE -->
                        <table>
                            <?php foreach ($products as $products) { ?>
                                <tr>
                                    <td>
                                        <div>
                                            <img src='img/<?= $products->id ?>.jpg' width='300 px' height='300 px'>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <a class="endTop__block_title"><?= $products->product_name ?></a>
                                            <p class="endTop__block_text"><?= $products->product_size ?></p>
                                            <p class="endTop__info_price">NT$<?= $products->price ?></p><br>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </section>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {

            let clickProduct = false;
            $('.product__li').click(function() {
                clickProduct = true;
                console.log(clickProduct);

                $('.product__imgPreview').addClass('nonDisp');
                $(this).find('.product__imgPreview').toggleClass('nonDisp');
                if ($(this).find('.product__imgPreview').hasClass('nonDisp')) {
                    $('.product__li_inner').css("opacity", "1");
                    $(this).find('.product__li_inner').css("opacity", "1");
                } else {
                    $('.product__li_inner').css("opacity", "0.5");
                    $(this).find('.product__li_inner').css("opacity", "0");
                }
            });
            $('.main').click(function() {
                if (!clickProduct) {
                    console.log(clickProduct);
                    $('.product__imgPreview').addClass('nonDisp');
                    $('.product__li_inner').css("opacity", "1");
                    $(this).find('.product__li_inner').css("opacity", "1");
                }
                clickProduct = false;
            });
        });
    </script>
</body>

</html>