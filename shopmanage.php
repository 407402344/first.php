<?php

require __DIR__ . '/etc/bootstrap.php';

//非管理者則跳回貼圖商店
if ($_SESSION["authority"] != "M") {
    header("Location:index-sup.php");
    die;
}

//獲取管理者名稱
$name = $_SESSION["name"] ?? "";

//檢查是否有排序及搜尋條件 
$sort = $_GET["sort"] ?? "id";
$search = $_GET["search"] ?? "";
$field = $_GET["field"] ?? "shop_id";

//獲取使用者清單欄位名稱 及使用者清單
$shopTitle = [
    "id"             => "編號",
    "shop_id"      => "商店編號",
    "shop_name"      => "分店名稱",
    "shop_address"   => "分店地址",
    "shop_phone"     => "分店電話",
    "updated_at"     => "更新日期",
];

$shopList = findShopLikeSearches($conn, $search, $field, $sort);

$editNum = $_GET["edit"] ?? $_POST["edit"] ?? '0';
$editId = $editNum - 1 ;
$editshop = findShopByIds($conn, $editNum);

$deleteNum = $_GET["delete"] ?? $_POST["delete"] ?? '0';
$deleteId = $deleteNum - 1 ;
$deleteshop = findShopByIds($conn, $deleteId);

$photo= $_GET["photo"] ?? ''; 
$result = isset($_GET["result"]) ? $_GET["result"] ? '成功' : '失敗' : '';
$msg = $_GET["msg"] ?? "";
$deleteId = $_GET["delete"] ?? $_POST["delete"] ?? '0';
$createId = $_GET["create"] ?? $_POST["create"] ?? '0';
?>

<html>

<?php include("header.php") ?>

<body class="page__userTable">
    <div class="wrapper">

        <header class="header">
            <h1 class="header__logo">
                <a href="index-sup.php"><span>WEB</span>STORE</a>
            </h1>
            <div class="header__search" data-widget="SearchBox">
                <form action="shopmanage.php" method="GET">
                    <span class="header__search_block header__search_block--filter">
                        <i class="material-icons icon-filter">filter_list</i>
                        <select class="search__filter_select" name="field">
                            <option value="shop_name">名稱</option>
                        </select>
                    </span>
                    <span class="header__search_block">
                        <input class="search__submit_input" type="submit" value="">
                        <i class="material-icons icon-search">search</i>
                        </input>
                    </span>
                    <input class="header__search_input" type="text" name="search" placeholder="搜尋商店" value=<?= $search ?>>
                </form>
            </div>
            <ul class="header__util">
                <li class="header__util_item wish-box">
                    <a><span>你好，<?= $name ?></span></a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="index-sup.php">
                        <span>返回商店</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="managepage.php">
                        <span>註冊清單管理</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="productmanage.php">
                        <span>產品管理</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item">
                    <a href="allmanage.php">
                        <span>概況一覽</span>
                    </a>
                    <span class="util__item_line">|</span>
                </li>
                <li class="header__util_item login-button">
                    <a href="process/logout_process.php">登出</a>
                </li>
            </ul>
        </header>

        <div class="content">
            <!-- EDIT BOARD -->
            <?php if (isset($_GET["edit"])) { ?>
                <div class="class__modal class__edit">
                    <div class="class__board">
                        <div class="class__board_inner">
                            <div class="class__board_logo">
                                <h1 class="class__board_title">Edit</h1>
                            </div>
                            <?php if ($result) { ?>
                                <p class="class__board_notice"> 修改資料<?= $result ?></p>
                            <?php } 
                                    if ($photo) {?>
                            
                                <p class="class__board_notice"> <?= $photo ?></p>
                            <?php } ?>

                            <div class="class__board_block">
                                <form class="class__form" name="updateForm" action="process/shopedit_process.php" method="post"  enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="<?= $editshop['id'] ?>">
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店編號</label>
                                        <input type="text" name="shop_id" placeholder="修改編號" value="<?= $editshop['shop_id'] ?>" required>
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店名稱</label>
                                        <input type="text" name="shop_name" placeholder="修改名稱" value="<?= $editshop['shop_name'] ?>" required>
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店地址</label>
                                        <input type="text" name="shop_address" placeholder="修改地址" value="<?= $editshop['shop_address'] ?>" required>
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店電話</label>
                                        <input type="text" name="shop_phone" placeholder="修改電話" value="<?= $editshop['shop_phone'] ?>" required>
                                    </div>
                                    <div class="class__form_textField">
                                            選擇圖片:
                                            <input type="file" name="fileToUpload" id="fileToUpload"><br>
                                    </div>
                                    <div class="class__form_btn">
                                        <button type="submit" class="btn submit__btn">修改</button>
                                        <button type="button" class="btn cancel__btn">
                                            <a href="shopmanage.php">取消</a>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (isset($_GET["delete"])) { ?>
                <div class="class__modal class__edit">
                    <div class="class__board">
                        <div class="class__board_inner">
                            <div class="class__board_logo">
                                <h1 class="class__board_title">Delete</h1>
                            </div>
                            <?php if ($result) { ?>
                                <p class="class__board_notice">此家分店刪除<?= $result ?></p>
                                <div class="class__board_block">
                                    <div class="class__form_btn">
                                        <button type="button" class="btn submit__btn">
                                            <a href="shopmanage.php">確認</a>
                                        </button>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <p class="class__board_text">是否確定要刪除此家分店<?= $result ?>？</p>
                                <div class="class__board_block">
                                    <form class="class__form" name="editForm" action="process/shopdelete_process.php" method="post">
                                        <input type="hidden" name="id" value="<?= $deleteshop['id']?>">
                                        <div class="class__form_btn">
                                            <button type="submit" class="btn submit__btn">確認</button>
                                            <button type="button" class="btn cancel__btn">
                                                <a href="shopmanage.php">取消</a>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($createId) { ?>
                <div class="class__modal class__edit">
                    <div class="class__board">
                        <div class="class__board_inner">
                            <div class="class__board_logo">
                                <h1 class="class__board_title">create</h1>
                            </div>
                            <?php if ($result) { ?>
                                <p class="class__board_notice"> 新增分店<?= $result?></p>
                            <?php } ?>
                            <?php if ($photo) { ?>
                                <p class="class__board_notice"> <?= $photo?></p>
                            <?php } ?>
                            <div class="class__board_block">
                                <form class="class__form" name="updateForm" action="process/shopcreate_process.php" method="post"  enctype="multipart/form-data">
                                
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店編號</label>
                                        <input type="text" name="shop_id" placeholder="請輸入分店編號" required autocapitalize="off" autocorrect="off" spellcheck="false">
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店名稱</label>
                                        <input type="text" name="shop_name" placeholder="請輸入分店名稱" required>
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店地址</label>
                                        <input type="text" name="shop_address" placeholder="請輸入分店地址" required>
                                    </div>
                                    <div class="class__form_textField">
                                        <label style="width: 100px;margin-top:10px" class="form__textField_label">分店電話</label>
                                        <input type="text" name="shop_phone" placeholder="請輸入分店電話" required>
                                    </div>
                                    <div class="class__form_textField">
                                            選擇圖片:
                                            <input type="file" name="fileToUpload" id="fileToUpload"><br>
                                    </div>
                                    <div class="class__form_btn">
                                        <button type="submit" class="btn submit__btn">新增</button>
                                        <button type="button" class="btn cancel__btn">
                                            <a href="shopmanage.php">取消</a>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="class__paper">
                <table class="class__table">
                    <thead class="class__table_head">
                        <tr class="class__table_row">
                            <?php foreach ($shopTitle as $key => $title) { ?>
                                <th class="class__table_cell class__table_cell--head">
                                    <a class="icon table__cell_button" href="shopmanage.php?sort=<?= $key ?>&search=<?= $search ?>&field=<?= $field ?>">
                                        <?= $title ?>
                                        <i class="material-icons">expand_more</i>
                                    </a>
                                </th>
                            <?php } ?>
                            <th class="class__table_cell class__table_cell--head table__cell--icon">EDIT</th>
                            <th class="class__table_cell class__table_cell--head table__cell--icon">DELETE</th>
                        </tr>
                    </thead>

                    <tbody class="class__table_content">
                        <?php foreach ($shopList as $key => $shop) { ?>
                            <tr class="class__table_row class__table_row--body">
                                <?php foreach ($shopTitle as $field => $title) { ?>
                                    <td class="class__table_cell class__table_cell--body">
                                        <?= $shop->$field ?>
                                    </td>
                                <?php } ?>
                                <td class="class__table_cell class__table_cell--body table__cell--icon">
                                    <form action="shopmanage.php?edit=<?= $shop->id ?>" method="post">
                                        <a class="table__cell_button">
                                            <input type="submit" name="edit" value="<?= $shop->id ?>">
                                            <i class="material-icons">edit</i>
                                            </input>
                                        </a>
                                    </form>
                                </td>
                                <td class="class__table_cell class__table_cell--body table__cell--icon">
                                    <form action="shopmanage.php?delete=<?= $shop->id ?>" method="post">
                                        <a class="table__cell_button">
                                            <input type="submit" name="delete" value="<?= $shop->id ?>">
                                            <i class="material-icons icon-delete">delete</i>
                                            </input>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="class__form_btn">
                    <form action="shopmanage.php?create=<?= $key ?>" method="post">
                        <button type="submit" style="margin-bottom:20px;background-color:#28FF28" class="btn submit__btn" name="create"> + 新增分店</button>
                    </form>
                </div>
                <div style="text-align:center;margin-bottom:10px">
                    <?php if ($msg) { ?>
                        <p style="color: #a8536c"> <?= $msg ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
</body>

</html>